# React Complete Guide

![Project-Course](https://img.shields.io/badge/Project-Course-yellow.svg)
![Status-In_Progress](https://img.shields.io/badge/Status-In_progress-brightgreen.svg)
![Maintained-Yes](https://img.shields.io/badge/Maintained-Yes-brightgreen.svg)
<a href="https://github.com/BorjaG90/react-complete-guide" alt="GitHub Repository Link">
<img alt="github repo" src="https://img.shields.io/badge/github-black?logo=github"/>
</a>
<a href="https://es.reactjs.org/" alt="Documentation Link">
<img alt="Documentation Link" src="https://img.shields.io/badge/Made_with-React-61dbfb"/>
</a>

Code and Exercises of the course React - The Complete Guide of Maximillian Schwarzmüller - Academind on Udemy

<https://www.udemy.com/course/react-the-complete-guide-incl-redux/>

This repo conforms to the course update of May 3, 2021.

## Built with

### Technologies

<a href="https://www.javascript.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/javascript.jpeg" width=50 alt="JavaScript"></a>
<a href="https://es.reactjs.org/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/reactJs.png" width=50 alt="React"></a>
<a href="https://styled-components.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/styledComponents.png" width=50 alt="StyledComponents"></a>

### Platforms

<a href="https://code.visualstudio.com/"><img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/vscode.png" width=50 alt="VSCode"></a>

## Authors

- **Borja Gete**

<a href="mailto:borjag90dev@gmail.com" alt="Borja Gete mail"><img src="https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail" title="Go To mail" alt="Borja Gete mail"/></a>
<a href="https://github.com/BorjaG90" alt="Borja Gete Github"><img src="https://img.shields.io/badge/BorjaG90-black?style=for-the-badge&logo=github" title="Go To Github Profile" alt="Borja Gete Github"/></a>
<a href="https://linkedin.com/in/borjag90" alt="Borja Gete LinkedIn"><img src="https://img.shields.io/badge/BorjaG90-blue?style=for-the-badge&logo=linkedin" title="Go To LinkedIn Profile" alt="Borja Gete LinkedIn"/></a>
