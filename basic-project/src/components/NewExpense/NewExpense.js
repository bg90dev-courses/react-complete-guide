import { useState } from "react";

import ExpenseForm from "./ExpenseForm/ExpenseForm";

import "./NewExpense.css";

const NewExpense = (props) => {
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
    setToggleShowForm(false);
  };

  const [showFormToggle, setToggleShowForm] = useState(false);

  const showFormHandler = () => {
    setToggleShowForm(true);
  };

  const hideFormHandler = () => {
    setToggleShowForm(false);
  };

  return (
    <div className="new-expense">
      {!showFormToggle && (
        <button onClick={showFormHandler}>Add New Expense</button>
      )}
      {showFormToggle && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          onCancel={hideFormHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;
