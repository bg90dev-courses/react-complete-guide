import "./ExpenseDate.css";

function ExpenseDate(props) {
  const day = props.date.toLocaleString("es-ES", { month: "2-digit" });
  const month = props.date.toLocaleString("es-ES", { month: "long" });
  const year = props.date.getFullYear();
  return (
    <div className="expense-date">
      <div className="expense-date_month">{month}</div>
      <div className="expense-date_year">{year}</div>
      <div className="expense-date_day">{day}</div>
    </div>
  );
}

export default ExpenseDate;
