import { useState } from "react";

import ExpensesFilter from "../ExpensesFilter/ExpensesFilter";
import Card from "../UI/Card/Card";
import ExpensesList from "./ExpensesList/ExpensesList";

import "./Expenses.css";
import ExpensesChart from "../ExpensesChart/ExpensesChart";

const Expenses = (props) => {
  const [filteredYear, setFilteredYear] = useState("2020");
  const filterChangeHandler = (selectedYear) => {
    setFilteredYear(selectedYear);
  };
  const filteredExpenses = props.expenses.filter((expense) => {
    return expense.date.getFullYear().toString() === filteredYear;
  });

  return (
    <Card className="expenses">
      <ExpensesFilter
        onFilterChanged={filterChangeHandler}
        selected={filteredYear}
      />
      <ExpensesChart expenses={filteredExpenses} />
      <ExpensesList expenses={filteredExpenses} />
    </Card>
  );
};

export default Expenses;
